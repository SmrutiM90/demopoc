#!/bin/python
import os
import re
import subprocess
import requests

# Getting list of repositories from GitLab
def list_gitlab_repos():
    repositories = []
    page = 1
    while True:
        params = {"page": page, "per_page": 100, "private_token": "cYhTXSLos2Zx4eELrtvF"}
        url = os.path.join("https://gitlab.com/api/v4", "projects")
        res = requests.get(url, params=params)
        repositories += res.json()
        if page >= int(res.headers["x-total-pages"]):
            break
        page += 1
    return repositories

print(list_gitlab_repos())