#!/bin/bash
set -e
rm -rf migration
repos=()

totalPages=$(curl --head --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&owned=yes" | grep x-total-pages | grep -o -e [0-9])
echo $totalPages
for ((i=1;i<=$totalPages;i++)); do
    repos+=$(curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&page=$i&owned=yes" --header "Content-Type: application/json" | jq '.[].http_url_to_repo')
done
echo $repos

for repo in $repos; do
    echo $repo
    repoName=$(echo $repo|rev|cut -d'.' -f2-|cut -f1 -d'/'|rev)
    echo $repoName
    projName=$(echo "${repo#*.com\/}"|rev|cut -d'/' -f2-|rev)
    echo $projName
    projID=$(echo "${repo#*.com\/}"|rev|cut -d'/' -f2-|rev|sed 's/\//_/g')
    echo $projID
    grpName=$(echo "${repo#*.com\/}"|rev|sed 's|.*\/||'|rev)
    echo $grpName
    repoURL=$(echo $repo|sed "s/\"//g")
    echo $repoURL
    gRepoURL=$(echo "${repo#*.com\/}"|sed "s/\"//g"|rev|cut -d'/' -f2-|rev|sed "s/\//%2F/g")
    echo $gRepoURL
    gRepoID=$(curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}" -H "Content-Type: application/json" | jq '.id')
    echo $gRepoID
    #FYI: -s = Don't show download progress, -o /dev/null = don't display the body, -w "%{http_code}" = Write http response code to stdout after exit
    projStatus=$(curl -s -o /dev/null -I -u che10_devops:gitlabMigration -w "%{http_code}" "https://api.bitbucket.org/2.0/workspaces/{che10_devops}/projects/$projID")
    echo $projStatus
    if [ $projStatus -eq 200 ]
    then
        echo "Project with this key:$projID  already exists in Bitbucket."
    else
        bproj=$(cat << EOF
{
    "name": "${projName}",
    "key": "${projID}",
    "description": "Software for colonizing mars.",
    "is_private": false
}
EOF
)
        curl -X POST -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/workspaces/{che10_devops}/projects" -H "Content-Type: application/json" -d "$bproj" | jq .
    fi
    mkdir migration
    cd migration
    git clone --bare $repoURL
    repoStatus=$(curl -s -o /dev/null -I -u che10_devops:gitlabMigration -w "%{http_code}" "https://api.bitbucket.org/2.0/repositories/{che10_devops}/$repoName")
    echo $repoStatus
    if [ "$repoStatus" -eq 200 ]
    then
        echo "Repository:$repoName already exists in Bitbucket"
    else
        brepo=$(cat << EOF
{
    "scm": "git",
    "is_private": false,
    "project": {
        "key": "${projID}"
    }
}
EOF
)
        curl -X POST -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/repositories/{che10_devops}/$repoName" -H "Content-Type: application/json" -d "$brepo" | jq .
        fi
    cd $repoName.git
    ls
    git push --mirror https://che10_devops:gitlabMigration@bitbucket.org/che10_devops/$repoName.git
    cd ..
    rm -rf $repoName.git
    cd ..
    rm -rf migration
    #validation
    # Validating commits:
    declare -a bCommit=()
    bCommitCount=0
    bCount=$(curl -X GET -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/repositories/{che10_devops}/{$repoName}/commits/?pagelen=100" -H "Content-Type: application/json" | jq '.next')
    if [[ $bCount == "null" ]]
    then
        bCommit+=$(curl -X GET -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/repositories/{che10_devops}/{$repoName}/commits/?pagelen=100" -H "Content-Type: application/json" | jq '.values[].hash')
        for commit in $bCommit
        do 
            ((bCommitCount+=1))
        done
            echo "$bCommitCount"
    else
        count=1
        while :
        do
            pageCount=$(curl -X GET -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/repositories/{che10_devops}/{$repoName}/commits/?pagelen=100" -H "Content-Type: application/json" | jq '.next')
            if [[ $pageCount == "null" ]]
            then
                 break
            else
                bCommit+=$(curl -X GET -u che10_devops:gitlabMigration "https://api.bitbucket.org/2.0/repositories/{che10_devops}/{$repoName}/commits/?pagelen=100&page=$count" -H "Content-Type: application/json" | jq '.values[].hash')
                count+=1
            fi
        done
            for commit in $bCommit
            do
                ((bCommitCount+=1))
            done
            echo "$bCommitCount"
    fi
    declare -a gCommitcount=()
    echo "in the api call for gitlab"
    gNextPage=$(curl -I -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}/repository/commits?per_page=100" | grep x-next-page | grep -o -e [0-9])
    if [ -z "$gnextPage" ]
    then
        gCommitCount+=$(curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}/repository/commits?all&per_page=100" -H "Content-Type: application/json" | jq '.[].id')
        gCommitCountNum=$(echo "${#gCommitCount[@]}")
        echo "${#gCommitCount[@]}"
    else
        while :
        do
            gNextPageCount=$(curl -I -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}/repository/commits?per_page=100"  | grep x-next-page | grep -o -e [0-9])
            if [ -z "$gNextPageCount" ]
            then
                break
            fi
                gCommitCount+=$(curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}/repository/commits?all&per_page=100&page=1" -H "Content-Type: application/json" | jq '.[].id')
                gCommitCount+=$(curl -X GET --header "PRIVATE-TOKEN: b568wxz6uT9Cb4uttKDC" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}/repository/commits?all&per_page=100&page=$count" -H "Content-Type: application/json" | jq '.[].id')
                gCommitCountNum=$(echo "${#gCommitCount[@]}")
                echo "${#gCommitCount[@]}"
        done
    fi
    if [ $bCommitCount -eq $gCommitCountNum ]
        then
            echo "Number of commits before and after migration are equal."
        else
            echo $repoName, $repoURL >> failedrepos.txt
            #echo "Repository: $repoName failed to migrate, deleting the repository from Bitbucket"
            #curl -s -o /dev/null -I -X DELETE -u che10_devops:gitlabMigration -w "%{http_code}" "https://api.bitbucket.org/2.0/repositories/{che10_devops}/$repoName"
            continue
        fi
done